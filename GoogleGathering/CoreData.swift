//
//  CoreData.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/22/22.
//
import Foundation
import CoreData

public protocol CoreDataModelProtocol: Any {

    var id: String { get }
}

public extension CoreDataModelProtocol {

    var entityName: String {
        return String(String(describing: type(of: self)).split(separator: ".").first!.dropLast(4))
    }

    func deleteFormDataBase() {
        CoreData.shared.deleteData(self)
    }

    func updateDataBase() {
        CoreData.shared.updateData(self)
    }
}

class CoreData {

    private static var instance: CoreData?
    static var shared: CoreData {
        get {
            if instance == nil {
                instance = CoreData()
            }
            return instance!
        }
        set {
            instance = newValue
        }
    }

    private var _persistentContainer: NSPersistentContainer?

    var persistentContainer: NSPersistentContainer {
        if _persistentContainer == nil {
            _persistentContainer = createPersistentContainer()
        }
        return _persistentContainer!
    }
}

// MARK: - internal
extension CoreData {

    func saveData(_ object: CoreDataModelProtocol) {

        let managedContext = persistentContainer.viewContext
        let dataEntity = NSEntityDescription.entity(forEntityName: object.entityName, in: managedContext)!
        let data = NSManagedObject(entity: dataEntity, insertInto: managedContext)
        let mirror = Mirror(reflecting: object)
        mirror.children.forEach { (label, value) in
            data.setValue(value, forKeyPath: label!)
        }
        do {
            try managedContext.save()
        } catch let error as NSError {
#if DEBUG
            print("Could not save. \(error), \(error.userInfo)")
#endif
        }
    }

    func fetchData<T>() -> [T]? {
        let managedContext = CoreData.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
        do {
            let result = try managedContext.fetch(fetchRequest)
            return result as? [T]
        } catch let error as NSError {
#if DEBUG
            print("Could not fetch. \(error), \(error.userInfo)")
#endif
            return nil
        }
    }

    func updateData(_ object: CoreDataModelProtocol) {

        let managedContext = persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: object.entityName)
        fetchRequest.predicate = NSPredicate(format: "id = %@", object.id)
        do {
            let fetchObject = try managedContext.fetch(fetchRequest)
            if !fetchObject.isEmpty, let objectUpdate = fetchObject.first as? NSManagedObject {
                let mirror = Mirror(reflecting: object)
                mirror.children.forEach { (label, value) in
                    objectUpdate.setValue(value, forKeyPath: label!)
                }
                do {
                    try managedContext.save()
                } catch let error as NSError {
#if DEBUG
                    print("Could not save for update. \(error), \(error.userInfo)")
#endif
                }

            } else {
                saveData(object)
            }
        } catch let error as NSError {
#if DEBUG
            print("Could not fetch for update. \(error), \(error.userInfo)")
#endif
        }
    }

    func deleteData(_ object: CoreDataModelProtocol) {

        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: object.entityName)
        fetchRequest.predicate = NSPredicate(format: "id = %@", object.id)
        do {
            let fetchObject = try managedContext.fetch(fetchRequest)
            guard !fetchObject.isEmpty, let objectToDelete = fetchObject.first as? NSManagedObject else { return }
            managedContext.delete(objectToDelete)
            do {
                try managedContext.save()
            } catch let error as NSError {
#if DEBUG
                print("Could not delete. \(error), \(error.userInfo)")
#endif
            }
        } catch let error as NSError {
#if DEBUG
            print("Could not fetch for delete. \(error), \(error.userInfo)")
#endif
        }
    }

    func clearCoreData() {
        for name in entityNames
        where ["Friend"].contains(name) {
            deleteObject(name)
        }
    }

    //     func clearCoreData() {
    //         let storeContainer = persistentContainer.persistentStoreCoordinator
    //         for store in storeContainer.persistentStores {
    //             do {
    //                 try storeContainer.destroyPersistentStore(
    //                     at: store.url!,
    //                     ofType: store.type,
    //                     options: nil
    //                 )
    //             } catch let error as NSError {
    // #if DEBUG
    //                 print("Could not delete. \(error), \(error.userInfo)")
    // #endif
    //             }
    //         }
    //         _persistentContainer = nil
    //     }
}

// MARK: - private
private extension CoreData {

    private var entityNames: [String] {
        let persistentContainer = CoreData.shared.persistentContainer
        var names: [String] = []
        persistentContainer.managedObjectModel.entities.forEach { entity in
            if let name = entity.name {
                names.append(name)
            }
        }
        return names
    }

    func deleteObject(_ entityName: String) {

        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do {
            let fetchObject = try managedContext.fetch(fetchRequest)
            guard !fetchObject.isEmpty, let objectsToDelete = fetchObject as? [NSManagedObject] else { return }
            for objectToDelete in objectsToDelete {
                managedContext.delete(objectToDelete)
            }
            do {
                try managedContext.save()
            } catch let error as NSError {
#if DEBUG
                print("Could not delete. \(error), \(error.userInfo)")
#endif
            }
        } catch let error as NSError {
#if DEBUG
            print("Could not fetch for delete. \(error), \(error.userInfo)")
#endif
        }
    }

    /// Re-create the persistent container
    /// and calling loadPersistentStores will re-create the persistent stores
    @discardableResult
    private func createPersistentContainer() -> NSPersistentContainer {
        let modelURL = Bundle.main.url(forResource: "GoogleGathering", withExtension: "momd")!
        let model = NSManagedObjectModel(contentsOf: modelURL)!
        let container = NSPersistentContainer(name: "GoogleGathering", managedObjectModel: model)
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }
}
