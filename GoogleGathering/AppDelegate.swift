//
//  AppDelegate.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/15/22.
//

import UIKit
import CoreData
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        GMSServices.provideAPIKey("AIzaSyCdoKXUxytwSXWuuL3TtnYrMiCt2WNJZJY")
        let viewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "TrackerViewController") as! TrackerViewController
        viewController.viewModel = TrackerViewModel({})
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
        return true
    }
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    /// Core Data Saving support
    func saveContext () {
        let context = CoreData.shared.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

