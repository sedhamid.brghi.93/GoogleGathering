//
//  AddFriendViewController.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/22/22.
//

import Foundation
import UIKit

class AddFriendViewController: UIViewController {
    @IBOutlet var saveButton: UIButton!
    var viewModel: AddFriendViewModelProtocol!
    @IBAction func nameChanged(_ sender: UITextField) {
        viewModel.name = sender.text ?? ""
        validate()
    }
    
    @IBAction func sureNameChanged(_ sender: UITextField) {
        viewModel.sureName = sender.text ?? ""
        validate()
    }
    
    func validate() {
        saveButton.isEnabled = !viewModel.name.isEmpty && !viewModel.sureName.isEmpty
    }
    @IBAction func save(_ sender: Any) {
        viewModel.save()
        dismiss(animated: true)
    }
}
