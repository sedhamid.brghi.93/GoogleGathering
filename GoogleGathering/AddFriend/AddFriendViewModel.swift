//
//  AddFriendViewModel.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/22/22.
//

import Foundation
import CoreLocation

protocol AddFriendViewModelProtocol {
    var name: String { get set }
    var sureName: String { get set }
    func save()
}

class AddFriendViewModel: AddFriendViewModelProtocol {
    func save() {
        saver()
    }
    var saver: () -> () = {}
    var name: String = ""
    var sureName: String = ""
}
