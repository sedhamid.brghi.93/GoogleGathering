//
//  TrackerViewModel.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/22/22.
//

import Foundation
import GoogleMaps
import CoreLocation

protocol TrackerViewModelProtocol {
    var location: CLLocation { get }
    var didLocationChange: (() -> Void) { get set }
    var friends: [FriendData] { get set }
    func save(location: CLLocationCoordinate2D, name: String, sureName: String)
    func viewDidLoad()
}

class TrackerViewModel: NSObject, TrackerViewModelProtocol, CLLocationManagerDelegate {
    var location: CLLocation
    var manager = CLLocationManager()
    var didLocationChange: (() -> Void)
    var friends: [FriendData] = []
    
    func viewDidLoad() {
        var dbFriends: [Friend]
        dbFriends = CoreData.shared.fetchData() ?? []
        friends = dbFriends.compactMap { $0.toData() }
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }
    func save(location: CLLocationCoordinate2D, name: String, sureName: String) {
        let friend = FriendData(id: UUID().uuidString,
                                latitude: location.latitude,
                                longitude: location.longitude,
                                firstName: name,
                                sureName: sureName)
        CoreData.shared.saveData(friend)
        friends.append(friend)
    }
    
    init(_ locationListener: @escaping (() -> Void)) {
        location = CLLocation(latitude: -33.86, longitude: 151.20)
        self.didLocationChange = locationListener
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
        didLocationChange()
    }
}

