//
//  FriendPinView.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/23/22.
//

import UIKit

class FriendPinView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var distanceLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    init() {
        super.init(frame: CGRect(origin: .zero, size: CGSize(width: 32, height: 64)))
        xibSetup()
    }
    
    func xibSetup() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: .main)
        nib.instantiate(withOwner: self, options: nil)
        logoView.tintColor = [UIColor.red, UIColor.blue, UIColor.yellow, UIColor.cyan, UIColor.brown, UIColor.green, UIColor.purple].randomElement()
        distanceLabel.textColor = logoView.tintColor
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView)
    }
}
