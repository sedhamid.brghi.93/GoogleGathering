//
//  Freind.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/22/22.
//

import Foundation

public struct FriendData: CoreDataModelProtocol {
    public var id: String
    public var latitude: Double
    public var longitude: Double
    public var firstName: String
    public var sureName: String
}
// MARK: - toData
public extension Friend {

    func toData() -> FriendData {
        .init(id: self.id!,
              latitude: self.latitude,
              longitude: self.longitude,
              firstName: self.firstName!,
              sureName: self.sureName!)
    }
}
