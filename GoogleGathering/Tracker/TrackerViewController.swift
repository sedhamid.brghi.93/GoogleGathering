//
//  ViewController.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/15/22.
//

import UIKit
import GoogleMaps

class TrackerViewController: UIViewController, GMSMapViewDelegate {
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var pinLogo: UIImageView!
    var viewModel: TrackerViewModelProtocol!
    var markers: [(GMSMarker, String)] = []
    var currentLoc: CLLocation?
    @IBOutlet var collectionView: UICollectionView!
    private var didMoveToMyLoc = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindVM()
    }
    
    func setupView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        collectionView.collectionViewLayout = layout
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.pinLogo.layer.shadowColor = UIColor.black.cgColor
        self.pinLogo.layer.shadowPath = CGPath(rect: CGRect(origin: CGPoint(x: 11, y: pinLogo.bounds.midY-5), size: CGSize(width: 8, height: 8)), transform: nil)
        self.pinLogo.layer.shadowOffset = CGSize(width: 0, height: 32)
        self.pinLogo.layer.shadowRadius = 4
        self.pinLogo.clipsToBounds = false
        self.pinLogo.layer.shadowOpacity = 0
    }
    
    func animateButtons() {
        UIView.animate(withDuration: 0.3) { [unowned self] in
            let isOpening = self.doneButton.isHidden
            self.addButton.transform = isOpening ? .init(rotationAngle: .pi/4) : .identity
            self.addButton.tintColor =  isOpening ? .red : #colorLiteral(red: 0.3153210282, green: 0.8400403309, blue: 0.89, alpha: 1)
            self.pinLogo.alpha = isOpening ? 1 : 0
            self.doneButton.isHidden.toggle()
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        UIView.animate(withDuration: 0.3, delay: 0.01, options: [.beginFromCurrentState]) { [unowned self] in
            var t = CGAffineTransform.identity
            t = t.translatedBy(x: 0, y: -16)
            t = t.scaledBy(x: 1.2, y: 1.2)
            self.pinLogo.layer.shadowOpacity = 1
            self.pinLogo.transform = t
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.pinLogo.transform = .identity
            self.pinLogo.layer.shadowOpacity = 0
        }
        
    }
    
    @IBAction func didTapAddOrCancel(_ sender: Any) {
        animateButtons()
    }
    
    func bindVM() {
        viewModel.viewDidLoad()
        collectionView.allowsMultipleSelection = true
        viewModel.didLocationChange = { [weak self] in
            guard let self = self else { return }
            if !self.didMoveToMyLoc {
                if let loc = self.mapView.myLocation {
                    let cam = GMSCameraPosition(target: loc.coordinate, zoom: 17)
                    self.mapView.animate(to: cam)
                    self.didMoveToMyLoc = true
                }
            }
            guard self.viewModel.location.coordinate != self.currentLoc?.coordinate else { return }
            self.currentLoc = self.viewModel.location
            for marker in self.markers {
                let distance = CLLocation(latitude: marker.0.position.latitude,
                                          longitude: marker.0.position.longitude)
                    .distance(from: self.viewModel.location)
                if let view =  marker.0.iconView as? FriendPinView {
                    view.distanceLabel.text = "\(distance.rounded())m"
                }
            }
        }
        collectionView.reloadData()
    }
    
    func addMarker(_ loc: CLLocationCoordinate2D,_ id: String) {
        let marker = GMSMarker(position: loc)
        marker.map = mapView
        let view = FriendPinView()
        marker.iconView = view
        if let current = self.currentLoc {
            let distance = CLLocation(latitude: loc.latitude, longitude: loc.longitude)
                .distance(from: current)
            view.distanceLabel.text = "\(distance.rounded())m"
        }
        markers.append((marker, id))
    }
    
    func clearMarkers() {
        markers = []
        mapView.clear()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let vc = segue.destination as? AddFriendViewController {
            animateButtons()
            let addViewModel = AddFriendViewModel()
            addViewModel.saver = { [unowned self] in
                self.clearMarkers()
                let loc = self.mapView.projection.coordinate(for: self.mapView.center)
                self.viewModel.save(location: loc, name: addViewModel.name, sureName: addViewModel.sureName)
                self.collectionView.reloadData()
            }
            vc.viewModel = addViewModel
            vc.preferredContentSize = CGSize(width: 250, height: 200)
            vc.popoverPresentationController?.delegate = self
        }
    }
    
}

extension TrackerViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension TrackerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendNameCell", for: indexPath) as! FriendNameCell
        let friend = viewModel.friends[indexPath.row]
        cell.nameLabel.text = friend.firstName + " " + friend.sureName
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let friend = viewModel.friends[indexPath.row]
        let coordinate = CLLocationCoordinate2D(latitude: friend.latitude, longitude: friend.longitude)
        let camera = GMSCameraPosition(target:  coordinate, zoom: mapView.camera.zoom)
        addMarker(coordinate, viewModel.friends[indexPath.row].id)
        mapView.animate(to: camera)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let friend = viewModel.friends[indexPath.row]
        if let markerIndex = markers.firstIndex(where: { $0.1 == friend.id }) {
            markers[markerIndex].0.map = nil
            markers.remove(at: markerIndex)
        }
    }
}
extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        lhs.longitude == rhs.longitude && lhs.latitude == lhs.latitude
    }
    
    
}
