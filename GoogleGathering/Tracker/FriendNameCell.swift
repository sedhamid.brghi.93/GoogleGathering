//
//  FriendNameCell.swift
//  GoogleGathering
//
//  Created by hamid baraghani on 9/23/22.
//

import UIKit

class FriendNameCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        let coloredView = UIView(frame: bounds)
        coloredView.backgroundColor = UIColor.yellow
        self.selectedBackgroundView = coloredView
        self.layer.cornerRadius = 15
    }
}
